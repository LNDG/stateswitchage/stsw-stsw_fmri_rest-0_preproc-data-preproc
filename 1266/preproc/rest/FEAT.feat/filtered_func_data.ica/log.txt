
Melodic Version 3.14

melodic -i /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/1266_rest_feat_detrended_bandpassed.nii.gz -o /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica --dimest=mdl -d 0 --tr=.645 --report --guireport=/home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/report.html --nobet --bgthreshold=3 --mmthresh=0.5 --bgimage=/home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/anat2func.nii.gz -v --Ostats 
---------------------------------------------

Melodic results will be in /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica

Create mask ... done
Reading data file /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/1266_rest_feat_detrended_bandpassed  ...  done
  Estimating data smoothness ...  done 
  Removing mean image ... done
  Normalising by voxel-wise variance ... done
Excluding voxels with constant value ... done

  Data size : 988 x 59226

Starting PCA  ... done
Start whitening using  24 dimensions ... 
  retaining 77.0564 percent of the variability 
 ... done

Starting ICA estimation using symm

  Step no. 1 change : 0.884446
  Step no. 2 change : 0.162436
  Step no. 3 change : 0.0510579
  Step no. 4 change : 0.0326172
  Step no. 5 change : 0.0300815
  Step no. 6 change : 0.0192547
  Step no. 7 change : 0.0119626
  Step no. 8 change : 0.00796674
  Step no. 9 change : 0.00708028
  Step no. 10 change : 0.00752334
  Step no. 11 change : 0.00700275
  Step no. 12 change : 0.00528103
  Step no. 13 change : 0.00357224
  Step no. 14 change : 0.00280542
  Step no. 15 change : 0.0026342
  Step no. 16 change : 0.00295939
  Step no. 17 change : 0.00297207
  Step no. 18 change : 0.00263382
  Step no. 19 change : 0.00208237
  Step no. 20 change : 0.00177924
  Step no. 21 change : 0.00165853
  Step no. 22 change : 0.00155128
  Step no. 23 change : 0.00145306
  Step no. 24 change : 0.00135034
  Step no. 25 change : 0.0012325
  Step no. 26 change : 0.00109673
  Step no. 27 change : 0.000948126
  Step no. 28 change : 0.000796646
  Step no. 29 change : 0.000655282
  Step no. 30 change : 0.00069941
  Step no. 31 change : 0.000723308
  Step no. 32 change : 0.000723303
  Step no. 33 change : 0.000698697
  Step no. 34 change : 0.000652256
  Step no. 35 change : 0.000589679
  Step no. 36 change : 0.00051822
  Step no. 37 change : 0.000445043
  Step no. 38 change : 0.00037591
  Step no. 39 change : 0.000322121
  Step no. 40 change : 0.000335956
  Step no. 41 change : 0.000350066
  Step no. 42 change : 0.000363621
  Step no. 43 change : 0.000375558
  Step no. 44 change : 0.000384628
  Step no. 45 change : 0.000389499
  Step no. 46 change : 0.000388905
  Step no. 47 change : 0.000381848
  Step no. 48 change : 0.000367803
  Step no. 49 change : 0.000346884
  Step no. 50 change : 0.000319904
  Step no. 51 change : 0.000288298
  Step no. 52 change : 0.00025392
  Step no. 53 change : 0.000218752
  Step no. 54 change : 0.000184609
  Step no. 55 change : 0.000152927
  Step no. 56 change : 0.000124647
  Step no. 57 change : 0.000100222
  Step no. 58 change : 7.9702e-05
  Step no. 59 change : 6.28456e-05
  Step no. 60 change : 4.92455e-05
  Convergence after 60 steps 

Sorting IC maps

Writing results to : 
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_IC
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_Tmodes
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_mix
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_FTmix
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_PPCA
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_ICstats
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/mask
...done
Creating report index page ...done


Running Mixture Modelling on Z-transformed IC maps ...
  IC map 1 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_1
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_1
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat1
   creating report page ...    done
  IC map 2 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_2
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_2
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat2
   creating report page ...    done
  IC map 3 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_3
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_3
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat3
   creating report page ...    done
  IC map 4 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_4
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_4
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat4
   creating report page ...    done
  IC map 5 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_5
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_5
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat5
   creating report page ...    done
  IC map 6 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_6
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_6
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat6
   creating report page ...    done
  IC map 7 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_7
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_7
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat7
   creating report page ...    done
  IC map 8 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_8
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_8
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat8
   creating report page ...    done
  IC map 9 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_9
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_9
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat9
   creating report page ...    done
  IC map 10 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_10
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_10
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat10
   creating report page ...    done
  IC map 11 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_11
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_11
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat11
   creating report page ...    done
  IC map 12 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_12
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_12
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat12
   creating report page ...    done
  IC map 13 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_13
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_13
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat13
   creating report page ...    done
  IC map 14 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_14
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_14
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat14
   creating report page ...    done
  IC map 15 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_15
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_15
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat15
   creating report page ...    done
  IC map 16 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_16
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_16
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat16
   creating report page ...    done
  IC map 17 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_17
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_17
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat17
   creating report page ...    done
  IC map 18 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_18
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_18
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat18
   creating report page ...    done
  IC map 19 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_19
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_19
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat19
   creating report page ...    done
  IC map 20 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_20
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_20
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat20
   creating report page ...    done
  IC map 21 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_21
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_21
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat21
   creating report page ...    done
  IC map 22 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_22
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_22
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat22
   creating report page ...    done
  IC map 23 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_23
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_23
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat23
   creating report page ...    done
  IC map 24 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_24
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_24
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat24
   creating report page ...    done

Writing results to : 
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_IC
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_Tmodes
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_mix
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_FTmix
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_PPCA
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_ICstats
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/mask
...done


 To view the output report point your web browser at /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1266/preproc/rest/FEAT.feat/filtered_func_data.ica/report/00index.html

finished!

