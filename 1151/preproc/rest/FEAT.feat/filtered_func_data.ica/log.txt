
Melodic Version 3.14

melodic -i /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/1151_rest_feat_detrended_bandpassed.nii.gz -o /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica --dimest=mdl -d 0 --tr=.645 --report --guireport=/home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/report.html --nobet --bgthreshold=3 --mmthresh=0.5 --bgimage=/home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/anat2func.nii.gz -v --Ostats 
---------------------------------------------

Melodic results will be in /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica

Create mask ... done
Reading data file /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/1151_rest_feat_detrended_bandpassed  ...  done
  Estimating data smoothness ...  done 
  Removing mean image ... done
  Normalising by voxel-wise variance ... done
Excluding voxels with constant value ... done

  Data size : 988 x 62527

Starting PCA  ... done
Start whitening using  22 dimensions ... 
  retaining 88.9095 percent of the variability 
 ... done

Starting ICA estimation using symm

  Step no. 1 change : 0.965222
  Step no. 2 change : 0.161485
  Step no. 3 change : 0.0860689
  Step no. 4 change : 0.0571245
  Step no. 5 change : 0.0239072
  Step no. 6 change : 0.0164391
  Step no. 7 change : 0.00860943
  Step no. 8 change : 0.00529685
  Step no. 9 change : 0.00486212
  Step no. 10 change : 0.00517639
  Step no. 11 change : 0.00524678
  Step no. 12 change : 0.00462357
  Step no. 13 change : 0.00344894
  Step no. 14 change : 0.00264816
  Step no. 15 change : 0.00217795
  Step no. 16 change : 0.00162513
  Step no. 17 change : 0.00111387
  Step no. 18 change : 0.000716884
  Step no. 19 change : 0.000443874
  Step no. 20 change : 0.000327131
  Step no. 21 change : 0.00029518
  Step no. 22 change : 0.000265931
  Step no. 23 change : 0.000239925
  Step no. 24 change : 0.000217396
  Step no. 25 change : 0.000209955
  Step no. 26 change : 0.00020739
  Step no. 27 change : 0.000215775
  Step no. 28 change : 0.000224302
  Step no. 29 change : 0.000231228
  Step no. 30 change : 0.000236256
  Step no. 31 change : 0.000239173
  Step no. 32 change : 0.000239858
  Step no. 33 change : 0.000241827
  Step no. 34 change : 0.000245945
  Step no. 35 change : 0.000248232
  Step no. 36 change : 0.000248714
  Step no. 37 change : 0.000247432
  Step no. 38 change : 0.000244444
  Step no. 39 change : 0.000239819
  Step no. 40 change : 0.000233645
  Step no. 41 change : 0.000226027
  Step no. 42 change : 0.00021709
  Step no. 43 change : 0.000206981
  Step no. 44 change : 0.000195868
  Step no. 45 change : 0.000183939
  Step no. 46 change : 0.000171399
  Step no. 47 change : 0.000158465
  Step no. 48 change : 0.000145356
  Step no. 49 change : 0.000132288
  Step no. 50 change : 0.000119463
  Step no. 51 change : 0.000107063
  Step no. 52 change : 9.52414e-05
  Step no. 53 change : 8.41222e-05
  Step no. 54 change : 7.37951e-05
  Step no. 55 change : 6.43172e-05
  Step no. 56 change : 5.57151e-05
  Step no. 57 change : 4.79884e-05
  Convergence after 57 steps 

Sorting IC maps

Writing results to : 
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_IC
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_Tmodes
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_mix
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_FTmix
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_PPCA
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_ICstats
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/mask
...done
Creating report index page ...done


Running Mixture Modelling on Z-transformed IC maps ...
  IC map 1 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_1
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_1
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat1
   creating report page ...    done
  IC map 2 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_2
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_2
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat2
   creating report page ...    done
  IC map 3 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_3
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_3
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat3
   creating report page ...    done
  IC map 4 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_4
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_4
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat4
   creating report page ...    done
  IC map 5 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_5
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_5
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat5
   creating report page ...    done
  IC map 6 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_6
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_6
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat6
   creating report page ...    done
  IC map 7 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_7
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_7
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat7
   creating report page ...    done
  IC map 8 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_8
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_8
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat8
   creating report page ...    done
  IC map 9 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_9
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_9
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat9
   creating report page ...    done
  IC map 10 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_10
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_10
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat10
   creating report page ...    done
  IC map 11 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_11
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_11
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat11
   creating report page ...    done
  IC map 12 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_12
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_12
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat12
   creating report page ...    done
  IC map 13 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_13
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_13
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat13
   creating report page ...    done
  IC map 14 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_14
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_14
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat14
   creating report page ...    done
  IC map 15 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_15
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_15
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat15
   creating report page ...    done
  IC map 16 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_16
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_16
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat16
   creating report page ...    done
  IC map 17 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_17
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_17
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat17
   creating report page ...    done
  IC map 18 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_18
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_18
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat18
   creating report page ...    done
  IC map 19 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_19
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_19
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat19
   creating report page ...    done
  IC map 20 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_20
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_20
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat20
   creating report page ...    done
  IC map 21 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_21
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_21
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat21
   creating report page ...    done
  IC map 22 ... 
   calculating mixture-model fit 
   saving probability map:    /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/probmap_22
   saving mixture model fit:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/MMstats_22
   re-scaling spatial maps ... 
   thresholding ... 
   alternative hypothesis test at p > 0.5
   saving thresholded Z-stats image:  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/stats/thresh_zstat22
   creating report page ...    done

Writing results to : 
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_IC
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_Tmodes
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_mix
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_FTmix
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_PPCA
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/melodic_ICstats
  /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/mask
...done


 To view the output report point your web browser at /home/mpib/LNDG/StateSwitch/WIP_rest/preproc/B_data/D_preproc/1151/preproc/rest/FEAT.feat/filtered_func_data.ica/report/00index.html

finished!

